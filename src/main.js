var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var size = 9
var tiles = create2DArray(size, size, 0, false)
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < tiles.length; i++) {
    for (var j = 0; j < tiles[i].length; j++) {
      noFill()
      stroke(255 * abs(sin(frame * 0.05)))
      strokeCap(ROUND)
      strokeWeight(boardSize * 0.01 * (15 / size))
      if (tiles[i][j] === 0) {
        push()
        translate(-(42 / 768) * boardSize * (15 / size) * 0.5 * sin(frame * 0.05), -(42 / 768) * boardSize * (15 / size) * 0.5)
        translate(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size))
        push()
        scale(sin(frame * 0.05), 1)
        arc(0, 0, (42 / 768) * boardSize * (15 / size), (42 / 768) * boardSize * (15 / size), 0, -Math.PI * 1.5)
        pop()
        pop()

        push()
        translate((42 / 768) * boardSize * (15 / size) * 0.5 * sin(frame * 0.05), (42 / 768) * boardSize * (15 / size) * 0.5)
        translate(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size))
        push()
        scale(sin(frame * 0.05), 1)
        arc(0, 0, (42 / 768) * boardSize * (15 / size), (42 / 768) * boardSize * (15 / size), Math.PI, Math.PI * 1.5)
        pop()
        pop()
      } else {
        push()
        translate((42 / 768) * boardSize * (15 / size) * 0.5, -(42 / 768) * boardSize * (15 / size) * 0.5 * sin(frame * 0.05))
        translate(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size))
        push()
        scale(1, sin(frame * 0.05))
        arc(0, 0, (42 / 768) * boardSize * (15 / size), (42 / 768) * boardSize * (15 / size), Math.PI * 0.5, Math.PI)
        pop()
        pop()

        push()
        translate(-(42 / 768) * boardSize * (15 / size) * 0.5, (42 / 768) * boardSize * (15 / size) * 0.5 * sin(frame * 0.05))
        translate(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size))
        push()
        scale(1, sin(frame * 0.05))
        arc(0, 0, (42 / 768) * boardSize * (15 / size), (42 / 768) * boardSize * (15 / size), -Math.PI * 0.5, 0)
        pop()
        pop()
      }
    }
  }

  frame += deltaTime * 0.01
  if (frame >= 125) {
    frame = 0
    tiles = create2DArray(size, size, 0, false)
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 2)
      }
    }
    array[i] = columns
  }
  return array
}
